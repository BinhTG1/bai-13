-- MySQL Workbench Synchronization
-- Generated: 2023-11-03 10:05
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: ACER

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `bai13`.`employee` (
  `employee_id` INT(10) UNSIGNED NOT NULL,
  `employee_fullname` VARCHAR(45) NULL DEFAULT NULL,
  `employee_birthday` DATE NULL DEFAULT NULL,
  `employee_phone` CHAR(14) NULL DEFAULT NULL,
  `employee_email` VARCHAR(45) NULL DEFAULT NULL,
  `employee_type` TINYINT(3) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`employee_id`),
  UNIQUE INDEX `Id_UNIQUE` (`employee_id` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `bai13`.`intern` (
  `intern_employee_id` INT(10) UNSIGNED NOT NULL,
  `intern_majors` VARCHAR(45) NULL DEFAULT NULL,
  `intern_semester` VARCHAR(45) NULL DEFAULT NULL,
  `intern_university_name` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`intern_employee_id`),
  INDEX `fk_intern_employee_idx` (`intern_employee_id` ASC) VISIBLE,
  CONSTRAINT `fk_intern_employee`
    FOREIGN KEY (`intern_employee_id`)
    REFERENCES `bai13`.`employee` (`employee_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `bai13`.`fresher` (
  `fresher_employee_id` INT(10) UNSIGNED NOT NULL,
  `fresher_graduation_date` DATE NULL DEFAULT NULL,
  `fresher_graduation_rank` VARCHAR(45) NULL DEFAULT NULL,
  `fresher_education` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`fresher_employee_id`),
  INDEX `fk_fresher_employee1_idx` (`fresher_employee_id` ASC) VISIBLE,
  CONSTRAINT `fk_fresher_employee1`
    FOREIGN KEY (`fresher_employee_id`)
    REFERENCES `bai13`.`employee` (`employee_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `bai13`.`experience` (
  `experience_employee_id` INT(10) UNSIGNED NOT NULL,
  `experience_exp_in_year` TINYINT(3) UNSIGNED NULL DEFAULT NULL,
  `experience_pro_skill` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`experience_employee_id`),
  CONSTRAINT `fk_experience_employee1`
    FOREIGN KEY (`experience_employee_id`)
    REFERENCES `bai13`.`employee` (`employee_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `bai13`.`certificate` (
  `certificate_id` INT(11) NOT NULL,
  `certificate_employee_id` INT(11) UNSIGNED NOT NULL,
  `certificate_name` VARCHAR(45) NULL DEFAULT NULL,
  `certificate_rank` VARCHAR(45) NULL DEFAULT NULL,
  `certificate_date` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`certificate_id`, `certificate_employee_id`),
  INDEX `fk_certificate_employee1_idx` (`certificate_employee_id` ASC) VISIBLE,
  CONSTRAINT `fk_certificate_employee1`
    FOREIGN KEY (`certificate_employee_id`)
    REFERENCES `bai13`.`employee` (`employee_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
