
public class Certificate {
	private Integer certificatedId;
	private String certificateName;
	private String certificateRank;
	private String certificateDate;
	
	
	Certificate() {
		
	}
	
	Certificate(Integer certificatedId, String certificateName, String certificateRank, String certificateDate) {
		setCertificatedId(certificatedId); 
		setCertificateName(certificateName);
		setCertificateRank(certificateRank);
		setCertificateDate(certificateDate);
	}
 	
	public Integer getCertificatedId() {
		return certificatedId;
	}
	public void setCertificatedId(Integer certificatedId) {
		this.certificatedId = certificatedId;
	}
	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	public String getCertificateRank() {
		return certificateRank;
	}
	public void setCertificateRank(String certificateRank) {
		this.certificateRank = certificateRank;
	}
	public String getCertificateDate() {
		return certificateDate;
	}
	public void setCertificateDate(String certificateDate) {
		this.certificateDate = certificateDate;
	}
	
	
}
