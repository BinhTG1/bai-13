import java.util.Set;

public abstract class Employee {
	private Integer id;
	private String fullName;
	private String birthDay;
	private String phone;
	private String email;
	private Byte employeeType;
	private static Integer employeeCount;
	private Set<Certificate> certificates;
	
	Employee() {
		
	}
	
	Employee(Integer id, String fullName, String birthDay, String phone, String email) {
		setId(id);
		setFullName(fullName);
		setBirthDay(birthDay);
		setPhone(phone);
		setEmail(email);
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getBirthDay() {
		return birthDay;
	}
	public void setBirthDay(String birthDay) {
		this.birthDay = birthDay;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Byte getEmployeeType() {
		return employeeType;
	}
	public void setEmployeeType(Byte employeeType) {
		this.employeeType = employeeType;
	}
	public static Integer getEmployeeCount() {
		return employeeCount;
	}
	public static void setEmployeeCount(Integer count) {
		Employee.employeeCount = count;
	}
	
	public void showInfo() {
		System.out.println(getId() + " | " + getFullName() + " | " + getBirthDay() + " | " + getPhone() + " | " + getEmail());
	}
	
	public abstract void showMe();

	public Set<Certificate> getCertificates() {
		return certificates;
	}

	public void setCertificates(Set<Certificate> certificates) {
		this.certificates = certificates;
	}
	
}
