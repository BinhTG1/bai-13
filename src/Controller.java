import java.util.Scanner;


public class Controller {
	private View view;
	private SQLConnection connection;
	
	enum Types {
		NAVIGATE,
		ADD,
		UPDATE,
		DELETE,
		SHOWINTERN,
		SHOWFRESHER,
		SHOWEXPERIENCE,
		ADDTYPE,
		ID,
		FULLNAME,
		BIRTHDAY,
		PHONE,
		EMAIL,
		STRING,
		NUMBER
		
	}
	
	Controller() {
		
	}
	Controller(View view, SQLConnection connection) {
		setView(view);
		setConnection(connection);
	}
	
	public void start() {
		Scanner scanner = new Scanner(System.in);
		Employee.setEmployeeCount(getConnection().getNumberOfEmployee());
		while (true) {
			getView().start();
			switch (Types.values()[Integer.parseInt(getUserInput(scanner, Types.NAVIGATE))]) {
				case ADD: {
					Employee tmp = null;
					System.out.println("Nhập id của nhân viên (nguyên dương): ");
					Integer id;
					while (true) {
						id = Integer.parseInt(getUserInput(scanner, Types.ID));
						if (getConnection().checkExist(id)) {
							System.out.println("ID này đã tồn tại.");
							continue;
						} else {
							break;
						}
					}
					System.out.println("Nhập tên của nhân viên (không kí tự đặc biệt hay số): ");
					String fullName = getUserInput(scanner, Types.FULLNAME);
					System.out.println("Nhập ngày sinh của nhân viên (YYYY-MM-DD): ");
					String birthDay = getUserInput(scanner, Types.BIRTHDAY);
					System.out.println("Nhập số điện thoại của nhân viên: ");
					String phone = getUserInput(scanner, Types.PHONE);
					System.out.println("Nhập email của nhân viên: ");
					String email = getUserInput(scanner, Types.EMAIL);
					getView().employeeType();
					switch (Integer.parseInt(getUserInput(scanner, Types.ADDTYPE))) {
						case 1: {
							System.out.println("Nhập chuyên ngành học: ");
							String majors = getUserInput(scanner, Types.STRING);
							System.out.println("Nhập chuyên học kì đang học: ");
							String semester = getUserInput(scanner, Types.STRING);
							System.out.println("Nhập chuyên tên trường đang học: ");
							String universityName = getUserInput(scanner, Types.STRING);
							tmp = new InternEmployee(id, fullName, birthDay, phone, email, majors, semester, universityName);
							break;
						}
						case 2: {
							System.out.println("Nhập thời gian tốt nghiệp: ");
							String graduationDate = getUserInput(scanner, Types.BIRTHDAY);
							System.out.println("Nhập xếp loại tốt nghiệp: ");
							String graduationRank = getUserInput(scanner, Types.STRING);
							System.out.println("Nhập trường tốt nghiệp: ");
							String education = getUserInput(scanner, Types.STRING);
							tmp = new FresherEmployee(id, fullName, birthDay, phone, email, graduationDate, graduationRank, education);
							break;
						}
						case 3: {
							System.out.println("Nhập số năm kinh nghiệm < 127: ");
							String expInYear = getUserInput(scanner, Types.NUMBER);
							System.out.println("Nhập kỹ năng chuyên môn: ");
							String proSkill = getUserInput(scanner, Types.STRING);
							tmp = new ExperienceEmployee(id, fullName, birthDay, phone, email,Byte.parseByte(expInYear), proSkill);
							break;
						}
					}
					connection.insertInto(tmp);
					break;
				}
				case UPDATE: {
					if (Employee.getEmployeeCount() == 0) {
						System.out.println("Chưa tồn tại dữ liệu nhân viên.");
						break;
					}
					System.out.println("Nhập id của nhân viên (nguyên dương): ");
					Integer id;
					while(true) {
						id = Integer.parseInt(getUserInput(scanner, Types.ID));
						if (!getConnection().checkExist(id)) {
							System.out.println("Nhân viên này không tồn tại.");
							continue;
						} else {
							break;
						}
					}
					Employee e = getConnection().getSpecific(id);
					e.showMe();
					System.out.println("Sửa thông tin id (Id cũ "+ id +"): ");
					Integer idTmp;
					while (true) {
						idTmp = Integer.parseInt(getUserInput(scanner, Types.ID));
						if (getConnection().checkExist(idTmp) && idTmp != id) {
							System.out.println("ID này đã tồn tại.");
							continue;
						} else {
							break;
						}
					}
					e.setId(idTmp);
					System.out.println("Sửa thông tin tên (Tên cũ " +e.getFullName()+ "): ");
					e.setFullName(getUserInput(scanner, Types.FULLNAME)); 
					System.out.println("Sửa thông tin ngày sinh (sinh nhật cũ "+e.getBirthDay()+"): ");
					e.setBirthDay(getUserInput(scanner, Types.BIRTHDAY));
					System.out.println("Sửa thông tin sđt (sđt cũ "+e.getPhone()+"): ");
					e.setPhone(getUserInput(scanner, Types.PHONE));
					System.out.println("Sửa thông tin email (email cũ " +e.getEmail()+ "): ");
					e.setEmail(getUserInput(scanner, Types.EMAIL));
					switch(e.getEmployeeType()) {
						case 0: {
							System.out.print("Sửa thông tin số năm kinh nghiệm (số năm kinh nghiệm cũ "+ ((ExperienceEmployee) e).getExpInYear() +"): ");
							((ExperienceEmployee) e).setExpInYear(Byte.parseByte(getUserInput(scanner, Types.NUMBER)));
							System.out.print("Sửa thông tin kĩ năng chuyên môn (kĩ năng chuyên môn cũ "+ ((ExperienceEmployee) e).getProSkill() +"): ");
							((ExperienceEmployee) e).setProSkill(getUserInput(scanner, Types.STRING));
							break;
						}
						case 1: {
							System.out.print("Sửa thông tin ngày tốt nghiệp (ngày tốt nghiệp cũ "+ ((FresherEmployee) e).getGraduationDate() +"): ");
							((FresherEmployee) e).setGraduationDate(getUserInput(scanner, Types.BIRTHDAY));
							System.out.print("Sửa thông tin xếp loại tốt nghiệp (xếp loại tốt nghiệp cũ "+ ((FresherEmployee) e).getGraduationRank() +"): ");
							((FresherEmployee) e).setGraduationRank(getUserInput(scanner, Types.STRING));
							System.out.print("Sửa thông tin trường tốt nghiệp (trường tốt nghiệp cũ "+ ((FresherEmployee) e).getEducation() +"): ");
							((FresherEmployee) e).setEducation(getUserInput(scanner, Types.STRING));
							break;
						}
						case 2: {
							System.out.print("Sửa thông tin chuyên ngành (chuyên ngành cũ "+ ((InternEmployee) e).getMajors() +"): ");
							((InternEmployee) e).setMajors(getUserInput(scanner, Types.STRING));
							System.out.print("Sửa thông tin học kì (học kì cũ "+ ((InternEmployee) e).getSemester() +"): ");
							((InternEmployee) e).setSemester(getUserInput(scanner, Types.STRING));
							System.out.print("Sửa thông tin trường đại học (trường đại học cũ "+ ((InternEmployee) e).getUniversityName() +"): ");
							((InternEmployee) e).setUniversityName(getUserInput(scanner, Types.STRING));
							break;
						}
					}
					connection.update(e, id);
					break;
				}
				case DELETE: {
					if (Employee.getEmployeeCount() == 0) {
						System.out.println("Chưa tồn tại dữ liệu nhân viên.");
						break;
					}
					System.out.println("Nhập id của nhân viên (nguyên dương): ");
					Integer id;
					while(true) {
						id = Integer.parseInt(getUserInput(scanner, Types.ID));
						if (!getConnection().checkExist(id)) {
							System.out.println("Nhân viên này không tồn tại.");
							continue;
						} else {
							break;
						}
					}
					getConnection().delete(id);
					
					break;
				}
				case SHOWINTERN: {
					getConnection().showAll((byte) 2);
					break;
				}
				case SHOWFRESHER: {
					getConnection().showAll((byte) 1);
					break;
				}
				case SHOWEXPERIENCE: {
					getConnection().showAll((byte) 0);
					break;
				}
			default:
				break;
			}
		
			
		}
		
		
	}
	
	public String getUserInput(Scanner scanner, Types type) {
		String tmp;
		while (true) {
			tmp = scanner.nextLine();
			switch(type) {
				case NAVIGATE : {
					
					try {
						Integer i = Integer.parseInt(tmp);
						if (i < 1 || i > 6) {
							throw new NumberFormatException();
						} else {
							return tmp;
						}
					} catch(NumberFormatException e) {
						System.out.println("Vui lòng nhập số từ 1 - 6.");
						
					}
					break;
				}
				case ADDTYPE: {
					try {
						Integer i = Integer.parseInt(tmp);
						if (i < 1 || i > 3) {
							throw new NumberFormatException();
						} else {
							return tmp;
						}
					} catch(NumberFormatException e) {
						System.out.println("Vui lòng nhập số từ 1 - 3.");
						
					}
					break;
				}
				
				case ID: {
					try {
						Integer.parseInt(tmp);
						return tmp;
							
					} catch(NumberFormatException e) {
						System.out.println("ID của nhân viên phải là số nguyên dương.");
						
					}
					break;
				}
				case FULLNAME: {
					
					try {
						if(!tmp.matches("^[a-zA-Z\\s]*$")) {
							throw new FullNameException("Tên không được phép có số hoặc kí tự đặc biệt.");
						} else {
							return tmp;
						}
					} catch(FullNameException e) {
						System.out.println(e.getMessage());
					}
					break;
				}
				case BIRTHDAY: {
					
					try {
						if(!tmp.matches("^\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$")) {
							throw new BirthDayException("Ngày không hợp lệ.");
						} else {
							return tmp;
						}
					} catch(BirthDayException e) {
						System.out.println(e.getMessage());
					}
					break;
				}
				case PHONE: {
					
					try {
						if(!tmp.matches("^\\d{7,13}$")) {
							throw new PhoneException("Số điện thoại không hợp lệ.");
						} else {
							return tmp;
						}
					} catch(PhoneException e) {
						System.out.println(e.getMessage());
					}
					break;
				}
				case EMAIL: {
					try {
						if(!tmp.matches("^.*[a-zA-Z0-9]@[a-zA-Z].*[.].*[a-zA-Z]")) {
							throw new EmailException("Email không hợp lệ.");
						} else {
							return tmp;
						}
					} catch(EmailException e) {
						System.out.println(e.getMessage());
					}
					break;
				}
				case STRING: {
					return tmp;
				}
				case NUMBER : {
					
					try {
						Integer.parseInt(tmp);
						return tmp;
					} catch(NumberFormatException e) {
						System.out.println("Vui lòng nhập số");
						
					}
					break;
				}
			default:
				break;
			}
		}
		
	}

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}
	public SQLConnection getConnection() {
		return connection;
	}
	public void setConnection(SQLConnection connection) {
		this.connection = connection;
	}
}
