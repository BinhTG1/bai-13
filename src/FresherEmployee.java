
public class FresherEmployee extends Employee {
	private String graduationDate;
	private String graduationRank;
	private String education;
	
	FresherEmployee() {
		
	}
	
	FresherEmployee(Integer id, String fullName, String birthDay, String phone, String email, String graduationDate, String graduationRank, String education) {
		super(id, fullName, birthDay, phone, email);
		setGraduationDate(graduationDate);
		setGraduationRank(graduationRank);
		setEducation(education);
		setEmployeeType((byte) 1);
	}

	@Override
	public void showMe() {
		// TODO Auto-generated method stub
		super.showInfo();
		System.out.println("Thời gian tốt nghiệp: " + getGraduationDate() + "\nXếp loại tốt nghiệp: " + getGraduationRank() + "\nTrường tốt nghiệp: " + getEducation() + "\n______________________________________________");
	}

	public String getGraduationDate() {
		return graduationDate;
	}

	public void setGraduationDate(String graduationDate) {
		this.graduationDate = graduationDate;
	}

	public String getGraduationRank() {
		return graduationRank;
	}

	public void setGraduationRank(String graduationRank) {
		this.graduationRank = graduationRank;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

}
