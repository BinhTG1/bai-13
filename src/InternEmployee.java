
public class InternEmployee extends Employee {
	private String majors;
	private String semester;
	private String universityName;
	
	InternEmployee() {
		
	}

	InternEmployee(Integer id, String fullName, String birthDay, String phone, String email, String majors, String semester, String universityName) {
		super(id, fullName, birthDay, phone, email);
		setMajors(majors);
		setSemester(semester);
		setUniversityName(universityName);
		setEmployeeType((byte) 2);
	}
	
	@Override
	public void showMe() {
		// TODO Auto-generated method stub
		super.showInfo();
		System.out.println("Chuyên ngành đang học: " + getMajors() + "\nHọc kì đang học: " + getSemester() + "\nTên trường đang học: " + getUniversityName() + "\n______________________________________________");
	}

	public String getMajors() {
		return majors;
	}

	public void setMajors(String majors) {
		this.majors = majors;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

}
