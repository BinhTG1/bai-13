
class BirthDayException extends Exception {
	public BirthDayException(String errorMessage) {
		super(errorMessage);
	}
}
class PhoneException extends Exception {
	public PhoneException(String errorMessage) {
		super(errorMessage);
	}
}
class EmailException extends Exception {
	public EmailException(String errorMessage) {
		super(errorMessage);
	}
}
class FullNameException extends Exception {
	 public FullNameException(String errorMessage) {
         super(errorMessage);
     }
	
}