
public class ExperienceEmployee extends Employee {
	private Byte expInYear;
	private String proSkill;
	
	ExperienceEmployee() {
		
	}
	
	ExperienceEmployee(Integer id, String fullName, String birthDay, String phone, String email, Byte expInYear, String proSkill) {
		super(id, fullName, birthDay, phone, email);
		setExpInYear(expInYear);
		setProSkill(proSkill);
		setEmployeeType((byte) 0);
	}

	@Override
	public void showMe() {
		// TODO Auto-generated method stub
		super.showInfo();
		System.out.println("Số năm kinh nghiệm: " + getExpInYear() + "\nKỹ năng chuyên môn: " + getProSkill() + "\n______________________________________________");
	}


	public Byte getExpInYear() {
		return expInYear;
	}


	public void setExpInYear(Byte expInYear) {
		this.expInYear = expInYear;
	}


	public String getProSkill() {
		return proSkill;
	}


	public void setProSkill(String proSkill) {
		this.proSkill = proSkill;
	}

}
