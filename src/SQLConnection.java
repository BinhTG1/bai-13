import java.sql.*;


public class SQLConnection {
	private Connection connection;
	private Statement statement;
	private PreparedStatement preparedStatement;
	
	SQLConnection() {
		setConnection("jdbc:mysql://localhost:3306/bai13", "root", "root");
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(String url, String username, String password) {
		try {
			this.connection = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Connection failed");
			e.printStackTrace();
		}
	}
	
	public void insertInto(Employee e) {
		if (e.equals(null)) {
			System.out.println("Smth went wrong when getting user input");
			return;
		}
		try {
			setPreparedStatement(getConnection().prepareStatement("Insert Into Employee Values(?, ?, ?, ?, ?, ?)"));
			getPreparedStatement().setInt(1, e.getId());
			getPreparedStatement().setString(2, e.getFullName());
			getPreparedStatement().setDate(3, Date.valueOf(e.getBirthDay()));;
			getPreparedStatement().setString(4, e.getPhone());
			getPreparedStatement().setString(5, e.getEmail());
			getPreparedStatement().setByte(6, e.getEmployeeType());
			int i = getPreparedStatement().executeUpdate();  
			System.out.println(i + " Inserted into Employee table");
			switch(e.getEmployeeType()) {
			case 0: {
				setPreparedStatement(getConnection().prepareStatement("Insert Into Experience Values(?, ?, ?)"));
				getPreparedStatement().setInt(1, e.getId());
				getPreparedStatement().setByte(2, ((ExperienceEmployee) e).getExpInYear());
				getPreparedStatement().setString(3, ((ExperienceEmployee) e).getProSkill());
				i = getPreparedStatement().executeUpdate();  
				System.out.println(i + " Inserted into Experience table");
				break;
			}
			case 1: {
				setPreparedStatement(getConnection().prepareStatement("Insert Into Fresher Values(?, ?, ?, ?)"));
				getPreparedStatement().setInt(1, e.getId());
				getPreparedStatement().setDate(2, Date.valueOf(((FresherEmployee) e).getGraduationDate()));
				getPreparedStatement().setString(3, ((FresherEmployee) e).getGraduationRank());
				getPreparedStatement().setString(4, ((FresherEmployee) e).getEducation());
				i = getPreparedStatement().executeUpdate();  
				System.out.println(i + " Inserted into Fresher table");
				break;
			}
			case 2: {
				setPreparedStatement(getConnection().prepareStatement("Insert Into Intern Values(?, ?, ?, ?)"));
				getPreparedStatement().setInt(1, e.getId());
				getPreparedStatement().setString(2, ((InternEmployee) e).getMajors());
				getPreparedStatement().setString(3, ((InternEmployee) e).getSemester());
				getPreparedStatement().setString(4, ((InternEmployee) e).getUniversityName());
				i = getPreparedStatement().executeUpdate();  
				System.out.println(i + " Inserted into Intern table");
				break;
			}
		}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	public void update(Employee e, Integer id) {
		try {
			setPreparedStatement(getConnection().prepareStatement("update employee set employee_id = ?, employee_fullname = ?, employee_birthday = ?, employee_phone = ?, employee_email = ? where employee_id = ?"));
			getPreparedStatement().setInt(1, e.getId());
			getPreparedStatement().setString(2, e.getFullName());
			getPreparedStatement().setDate(3, Date.valueOf(e.getBirthDay()));;
			getPreparedStatement().setString(4, e.getPhone());
			getPreparedStatement().setString(5, e.getEmail());
			getPreparedStatement().setInt(6, id);
			int i=getPreparedStatement().executeUpdate();  
			System.out.println(i+" records updated");  
			switch (e.getEmployeeType()) {
			case 0: {
				setPreparedStatement(getConnection().prepareStatement("update experience set experience_exp_in_year = ?, experience_pro_skill = ? where experience_employee_id = ?"));
				getPreparedStatement().setByte(1, ((ExperienceEmployee) e).getExpInYear());
				getPreparedStatement().setString(2, ((ExperienceEmployee) e).getProSkill());
				i = getPreparedStatement().executeUpdate();  
				System.out.println(i+" records in experience updated"); 
				break;
			}
			case 1: {
				setPreparedStatement(getConnection().prepareStatement("update fresher set fresher_graduation_date = ?, fresher_graduation_rank = ?, fresher_education where fresher_employee_id = ?"));
				getPreparedStatement().setDate(2, Date.valueOf(((FresherEmployee) e).getGraduationDate()));
				getPreparedStatement().setString(3, ((FresherEmployee) e).getGraduationRank());
				getPreparedStatement().setString(4, ((FresherEmployee) e).getEducation());
				i = getPreparedStatement().executeUpdate();  
				System.out.println(i + " records in fresher updated");
				break;
			}
			case 2: {
				setPreparedStatement(getConnection().prepareStatement("update intern set intern_majors = ?, intern_semester = ?, intern_university_name = ? where intern_employee_id = ?"));
				getPreparedStatement().setString(2, ((InternEmployee) e).getMajors());
				getPreparedStatement().setString(3, ((InternEmployee) e).getSemester());
				getPreparedStatement().setString(4, ((InternEmployee) e).getUniversityName());
				i = getPreparedStatement().executeUpdate();  
				System.out.println(i + " records in intern updated");
				break;
			}
		}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	public Integer getNumberOfEmployee() {
		try {
			setStatement(getConnection().createStatement());
			ResultSet rs = getStatement().executeQuery("select count(*) from employee");
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
	}
	
	public boolean checkExist(Integer id) {
		try {
			setPreparedStatement(getConnection().prepareStatement("Select employee_id From Employee Where employee_id = ?"));
			getPreparedStatement().setInt(1, id);
			ResultSet rs = getPreparedStatement().executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public Employee getSpecific(Integer id) {
		Employee result = null;
		try {
			setPreparedStatement(getConnection().prepareStatement("Select * From Employee Where employee_id = ?"));
			getPreparedStatement().setInt(1, id);
			ResultSet rs = getPreparedStatement().executeQuery();
			while (rs.next()) {
				switch(rs.getByte(6)) {
					case 0: {
						setPreparedStatement(getConnection().prepareStatement("Select * From Experience Where experience_employee_id = ?"));
						getPreparedStatement().setInt(1, id);
						ResultSet rs1 = getPreparedStatement().executeQuery();
						rs1.next();
						result = new ExperienceEmployee(rs.getInt(1), rs.getString(2), rs.getDate(3).toString(), rs.getString(4), rs.getString(5), rs1.getByte(2), rs1.getString(3));
						break;
					}
					case 1: {
						setPreparedStatement(getConnection().prepareStatement("Select * From Fresher Where fresher_employee_id = ?"));
						getPreparedStatement().setInt(1, id);
						ResultSet rs1 = getPreparedStatement().executeQuery();
						rs1.next();
						result = new FresherEmployee(rs.getInt(1), rs.getString(2), rs.getDate(3).toString(), rs.getString(4), rs.getString(5), rs1.getDate(2).toString(), rs1.getString(3), rs1.getString(4));
						break;
					}
					case 2: {
						setPreparedStatement(getConnection().prepareStatement("Select * From Intern Where intern_employee_id = ?"));
						getPreparedStatement().setInt(1, id);
						ResultSet rs1 = getPreparedStatement().executeQuery();
						rs1.next();
						result = new InternEmployee(rs.getInt(1), rs.getString(2), rs.getDate(3).toString(), rs.getString(4), rs.getString(5), rs1.getString(2), rs1.getString(3), rs1.getString(4));
						break;
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		try {
			setPreparedStatement(getConnection().prepareStatement("Delete From Employee Where employee_id = ?"));
			getPreparedStatement().setInt(1, id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void showAll(Byte type) {
		try {
			setPreparedStatement(getConnection().prepareStatement("Select * From Employee Where employee_type = ?"));
			getPreparedStatement().setByte(1, type);
			ResultSet rs = getPreparedStatement().executeQuery();
			while (rs.next()) {
				switch(type) {
					case 0: {
						setPreparedStatement(getConnection().prepareStatement("Select * From Experience Where experience_employee_id = ?"));
						getPreparedStatement().setInt(1, rs.getInt(1));
						ResultSet rs1 = getPreparedStatement().executeQuery();
						rs1.next();
						new ExperienceEmployee(rs.getInt(1), rs.getString(2), rs.getDate(3).toString(), rs.getString(4), rs.getString(5), rs1.getByte(2), rs1.getString(3)).showMe();;
						break;
					}
					case 1: {
						setPreparedStatement(getConnection().prepareStatement("Select * From Fresher Where fresher_employee_id = ?"));
						getPreparedStatement().setInt(1, rs.getInt(1));
						ResultSet rs1 = getPreparedStatement().executeQuery();
						rs1.next();
						new FresherEmployee(rs.getInt(1), rs.getString(2), rs.getDate(3).toString(), rs.getString(4), rs.getString(5), rs1.getDate(2).toString(), rs1.getString(3), rs1.getString(4)).showMe();;
						break;
					}
					case 2: {
						setPreparedStatement(getConnection().prepareStatement("Select * From Intern Where intern_employee_id = ?"));
						getPreparedStatement().setInt(1, rs.getInt(1));
						ResultSet rs1 = getPreparedStatement().executeQuery();
						rs1.next();
						new InternEmployee(rs.getInt(1), rs.getString(2), rs.getDate(3).toString(), rs.getString(4), rs.getString(5), rs1.getString(2), rs1.getString(3), rs1.getString(4)).showMe();;
						break;
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Statement getStatement() {
		return statement;
	}

	public void setStatement(Statement statement) {
		this.statement = statement;
	}

	public PreparedStatement getPreparedStatement() {
		return preparedStatement;
	}

	public void setPreparedStatement(PreparedStatement preparedStatement) {
		this.preparedStatement = preparedStatement;
	}

	
	
}
